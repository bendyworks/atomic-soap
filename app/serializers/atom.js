import DS from 'ember-data';
import Ember from 'ember';

export default DS.JSONSerializer.extend({
  extractArray: function(store, type, xmlDoc) {
    var $ = Ember.$;
    var xml = $(xmlDoc).find('GetAtomsResponse GetAtomsResult').text();
    var inner = $($.parseXML(xml)).find('NewDataSet Table');

    return inner.map(function(idx, el) {
      return {
        id: idx,
        name: $(el).find('ElementName').text()
      };
    }).get();
  }
});
