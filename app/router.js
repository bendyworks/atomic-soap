import Ember from 'ember';

var Router = Ember.Router.extend({
  location: AtomicSoapENV.locationType
});

Router.map(function() {
  this.resource('atoms', function() {
    this.route('atom', {path: '/:name'});
  });
});

export default Router;
