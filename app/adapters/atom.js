import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  headers: {
    'SOAPAction': 'http://www.webserviceX.NET/GetAtoms',
    'Content-Type': 'text/xml;charset=UTF-8'
  },

  /*jshint multistr: true */
  getAtomsRequestData: '<?xml version="1.0" encoding="UTF-8"?> \
  <env:Envelope xmlns:tns="http://www.webserviceX.NET" \
                xmlns:env="http://schemas.xmlsoap.org/soap/envelope/"> \
    <env:Body> \
      <tns:GetAtoms></tns:GetAtoms> \
    </env:Body> \
  </env:Envelope>',

  findAll: function() {
    return this.ajax('http://0.0.0.0:4200/periodictable.asmx',
                     'POST',
                     { data: this.getAtomsRequestData });
  },

  ajaxOptions: function(url, type, options) {
    var hash = DS.RESTAdapter.prototype.ajaxOptions.call(this, url, 'GET', options);
    hash.type = 'POST';
    hash.dataType = 'xml';
    return hash;
  }
});
